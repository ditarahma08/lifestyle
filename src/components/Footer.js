import styles from '@/styles/Footer.module.css'

const Footer = () => {
	return (
		<div className={`d-flex justify-content-center align-items-center px-5 py-3 ${styles.footer}`}>
			<div>
				<span>2023. Dita Rahma Puspitasari</span>
			</div>
		</div>
	)
}

export default Footer